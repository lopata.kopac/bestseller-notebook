# Bestseller notebook project

This project is intended for presentacion. We using React, Typescript, Eslint, Styled-compoment, Vite, Axios.

## Fist clone

We using `npm` for Javascript pacpages. After clone project you need run comand for instaling pacpages before start project:

```text
    npm i
```

Open http://localhost:3000/

Project use `LF` end of line. For remove errors with end of line you can run command for `eslint`.

```text
    npm run eslint:fix
```

## Run project

## Run Command

For run project run command:

```text
    npm start
```

### Setting port

Project will run on port `3000`, if you want cahnege port you neet go to `vite.config.ts` and change config:

```javascript
server: {
    port: 3000,
}
```

## VSCode

### Eslint

If you are using VSCode editon you can add setting for fir eslint format after save file. You need install eslint Extension. For add on change auto format setting to VSCode press `Ctrl + + Shift + P` or `Command + Shift + P` pick `Preferences: Open Workspace Settings(JSON)` and add this code to `.vscode/settings.json` file..

```JSON
{
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    },
    "eslint.validate": ["typescript", "typescriptreact"]
}
```

### Debuging

For debuging setting to VSCode press `Ctrl + + Shift + D` or `Command + Shift + D` select `create a launch.json` and add this code to `.vscode/launch.json` file. You need run project before debuging.

```JSON
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "chrome",
            "request": "launch",
            "name": "Launch Chrome against localhost",
            "url": "http://localhost:3000",
            "webRoot": "${workspaceFolder}"
        }
    ]
}
```
