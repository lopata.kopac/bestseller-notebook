import React from 'react';
import styled from 'styled-components';
import star from '../../assets/star.png';
import blackStar from '../../assets/empty-star.png';

interface Props{
    className?: string;
    key?: string
    name: string;
    note: string;
    price: string;
    image: string;
    rating: number;
    onClick?: ()=>void;
}

const SliderCardStyled = styled.div`
    max-width: 280px;
    padding: 0px 20px;

    .slider-card-image{
        width: 100%;
    }

    .slider-card-stars{
        img{
            width: 25px;
        }
    }

    .slider-card-price{
        color: #899C44;
    }

    .slider-card-name{
        margin: 4px 0;
    }

    .slider-card-note{
        margin: 8px 0;
    }

    .slider-card-price{
        margin: 8px 0;
    }
`;

function SliderCardRating({rating}: {rating: number}) {
    const ratingFloor = Math.floor(rating);
    const stars = [];

    for(let counter = 0; counter < 5; counter++){
        if(counter <= ratingFloor){
            stars.push( <img src={star} alt='star'/>);
        } else {
            stars.push( <img src={blackStar} alt='black star' />);
        }
    }
    
    return (
        <div className='slider-card-stars'>
            {stars}
        </div>
    );
}

function SliderCard({key, rating, className, image, name, note, price, onClick}: Props) {
    const maxNote = `${note.slice(0, 120)}${note.length>120?'...':''}`;
    return (
        <SliderCardStyled key={key} onClick={onClick} className={className}>
            <img className='slider-card-image' src={image} alt={name}/>
            <h4 className='slider-card-name'>{name}</h4>
            <SliderCardRating rating={rating}/>
            <p className='slider-card-note'>{maxNote}</p>
            <p className='slider-card-price'>{price}</p>
        </SliderCardStyled>
    );
}

export default SliderCard;
