import Container from './slider-container';
import Card from './slider-card';

const Slider = { Container, Card };

export default Slider;