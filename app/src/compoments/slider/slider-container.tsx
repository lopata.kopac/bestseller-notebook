import React from 'react';
import AliceCarousel from 'react-alice-carousel';
import styled from 'styled-components';

interface Props{
    children?: JSX.Element | JSX.Element[];
    className?: string;
    title?: string;
}

const responsive = {
    0: { items: 1 },
    568: { items: 3 },
    1024: { items: 5 },
};

const SliderStyled = styled.div`
    margin-top: 42px;

    .slider-title{
        color: #3D9FFA;
        margin: 10px 0px;
    }

    .alice-carousel{
        width: auto;
        padding-left: 32px;
        padding-right: 32px;
    }

    .alice-carousel__prev-btn{
        padding: 0;
    }

    .alice-carousel__next-btn{
        padding: 0;
    }
`;

const SliderButton = styled.div<{side: string}>`
    position: absolute;
    right: ${props => (props.side === 'next'? 0 : `auto`)};
    left: ${props => (props.side === 'prev'? 0 : `auto`)};
    top: 50%;
    transform: translate(0, -50%);

    background: #3D9FFA;
    color: white;

    width: 24px;
    height: 60px;

    border: 2px solid #222;
    border-radius: 8px;

    display: flex;
    justify-content: center;
    align-items: center;

    cursor: context-menu;
`;

const renderNextButton = () => <SliderButton side='next'>{'>'}</SliderButton>;

const renderPrevButton = () => <SliderButton side='prev'>{'<'}</SliderButton>;

function SliderContainer({children, className, title}: Props) {
  return (
    <SliderStyled className={className}>
        <h3 className='slider-title'>{title}</h3>
        <AliceCarousel
            infinite
            disableDotsControls
            mouseTracking
            items={children as any[]}
            responsive={responsive}
            controlsStrategy='alternate'
            renderPrevButton={renderPrevButton}
            renderNextButton={renderNextButton}
        />
    </SliderStyled>
  );
}

export default SliderContainer;
