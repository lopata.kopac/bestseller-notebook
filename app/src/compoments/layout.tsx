import React from 'react';
import styled from 'styled-components';

interface Props{
    children?: JSX.Element | JSX.Element[];
    className?: string;
    title?: string;
}

const StyledLayout = styled.div`
    padding: 10px 20px;

    .layout-title{
        color: #2644A4;
        margin: 10px 0px;
        margin-bottom: 32px;
    }
`;

function Layout({children, className, title}: Props) {
    return (
        <StyledLayout className={className}>
            <h1 className='layout-title'>{title}</h1>
            <section>{children}</section>
        </StyledLayout>
    );
  
}


export default Layout;
