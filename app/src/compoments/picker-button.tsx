import React from 'react';
import styled from 'styled-components';

interface Props{
    children?: JSX.Element | JSX.Element[] | string;
    className?: string;
    onClick?: () => void;
}

const PickerButton = styled.button`
    padding: 10px 10px;
    margin: 5px 10px;
    width: 250px;
    border: 1px solid #ccc;
    border-radius: 0;

    @media (max-width: 768px) {
        width: 46%;
    }
`;

export default function({children, className, onClick}: Props){
    return  <PickerButton className={className} onClick={onClick}>{children}</PickerButton>;
}