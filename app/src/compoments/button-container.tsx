import React from 'react';
import styled from 'styled-components';

interface Props{
    children?: JSX.Element | JSX.Element[] | string;
    className?: string;
}

const ButtonContainer = styled.div`
    display: inline-flex;
    flex-wrap: wrap;
    gap: 12px;

    padding-top: 6px;

    > button {
        margin: 0;
    }

    @media (max-width: 768px) {
        gap: 0;
        justify-content: space-between;

        padding-top: 0;

        > button{
            margin: 6px 0;
        }
    }
`;

export default function({children, className}: Props){
    return  <ButtonContainer className={className}>{children}</ButtonContainer>;
}