export enum RequestStatus {
    Init = 'INIT',
    Fetching = 'FETCHING',
    Error = 'ERROR',
    Done = 'DONE'
}