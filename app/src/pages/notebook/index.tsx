import React, { useContext } from 'react';
import NotebookDataProvider, { NotebookDataContext } from '../../app/notebook-data.contex';
import ButtonContainer from '../../compoments/button-container';
import Layout from '../../compoments/layout';
import PickerButton from '../../compoments/picker-button';
import Slider from '../../compoments/slider';

const pickerButtons = ['Macbook', 'Herny', 'Kancelárske', 'Profesionálne', 'Stylové', 
    'Základne', 'Dotykové', 'Na splátky', 'VR Ready', 'IRIS Graphics', 'Brašne, Batohy', 'Prislušenstvo'];

function SliderNotebookMap(data: any[]){
    return data
            .slice(0, 8)
            .map(item=>
                <Slider.Card 
                    key={item.name}
                    image={item.img}
                    name={item.name}
                    note={item.spec}
                    price={item.price}
                    rating={item.rating}
                ></Slider.Card>
            );
}

const Notebook = () => {
    const {data: notebooks} = useContext(NotebookDataContext);

    if(!notebooks){
        return <></>;
    }

    return (
        <>
            <ButtonContainer>
                {pickerButtons.map(picker=> 
                    <PickerButton
                        key={picker}
                        onClick={()=>console.log(picker)}
                    >{picker}</PickerButton>
                )}
            </ButtonContainer>

            <Slider.Container title='Najpredavenejší'>
                {SliderNotebookMap(notebooks)}
            </Slider.Container>
        </>
    );
};

export default function() {
    return (
        <NotebookDataProvider>
            <Layout
            title='Notebook'
            >
                <Notebook/>
            </Layout>
        </NotebookDataProvider>
    );
}