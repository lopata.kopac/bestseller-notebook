import './App.scss';
import Notebook from './pages/notebook';

function App() {

  return (
    <Notebook></Notebook>
  );
}

export default App;
