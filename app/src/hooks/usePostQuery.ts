import axios from "axios";
import { useEffect, useState } from "react";
import { RequestStatus } from "../untils/models";

export interface IResonseDara<T>{
    data: T[] | any[] | null,
    status: RequestStatus,
    error: any,
}

export interface IUseQueryResponse<T> extends IResonseDara<T>{
  refetch: (options?: { url?: string | undefined; body?: any; }) => void,
}

export function usePostQuery<T>(url: string, body: any): IUseQueryResponse<T>{
    const [response, setResponse] = useState<IResonseDara<T>>({
        status: RequestStatus.Init,
        data: null,
        error: null
    });

  const fetchData  = async (options?: {url?: string, body?: any}) => {
    setResponse((state)=>({...state, status: RequestStatus.Fetching}));

    try {
      const res = await axios.post(options?.url || url, options?.body || body);

      if(res.data.err > 0){
        setResponse((state)=>({...state, status: RequestStatus.Error, data: null, error: res.data.msg}));
      }
      else {
        setResponse((state)=>({...state, status: RequestStatus.Done, data: res.data.data}));
      }
    } catch (error) {
      setResponse((state)=>({...state, status: RequestStatus.Error, data: null, error}));
    }
  };

  useEffect(()=> {
      fetchData();
  }, []);

  return {
    refetch: fetchData,
    data: response.data,
    status: response.status,
    error: response.error
  };
}