import React, { useEffect, useLayoutEffect, useState } from 'react';
import { IUseQueryResponse, usePostQuery } from '../hooks/usePostQuery';
import { RequestStatus } from '../untils/models';

interface Props {
  children?: JSX.Element | JSX.Element[] | string;
}

export interface INotebookDataContext extends IUseQueryResponse<any>{
  setPage: (page: number) => void,
  setFilter: (filter: any) => void,
  page: number,
  filter: any,
}

export const NotebookDataContext = React.createContext<INotebookDataContext>(null);

const filterParameters = {
  id: 18855843,
  isInStockOnly: false,
  newsOnly: false,
  wearType: 0,
  orderBy: 0,
  page: 1,
  params: [{
      tId: 0,
      v: []
  }],
  producers: [],
  sendPrices:true,
  type: 'action',
};

function NotebookDataProvider({children}: Props) {
  const [filter, setFilter] = useState<any>({});
  const [page, setPage] = useState(1);

  const post = usePostQuery<any>(`${import.meta.env.VITE_API_URL}/products`, {
    filterParameters
  });

  const setPageHandle = (page: number)=>{
    setPage(page);
  };

  const setFilterHandle = (filter: any)=>{
    setPage(1);
    setFilter(filter);
  };

  useEffect(()=> {
    if(post.status !== RequestStatus.Init){
      post.refetch({body: {
        filterParameters: {
          ...filterParameters,
          page: page
        }
      }});
    }
  }, [page, filter]);

  return (
    <NotebookDataContext.Provider value={{
        setPage: setPageHandle,
        setFilter: setFilterHandle,
        page,
        filter,
        ...post
      }}>
      {children}
    </NotebookDataContext.Provider>
  );
}

export default NotebookDataProvider;